package com.github.sibmaks.cola;

/**
 * Cache oblivious lookahead array
 *
 * @author sibmaks
 * @since 0.0.1
 */
public interface COLA<K extends Comparable<K>, V> {

    /**
     * Returns the value to which the specified key is mapped, or {@code null} if this COLA contains no mapping for the key.
     * This method start search value from the largest array.
     *
     * @param key - the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or {@code null} if this COLA contains no mapping for the key
     */
    V getBottomUp(K key);

    /**
     * Returns the value to which the specified key is mapped, or {@code null} if this COLA contains no mapping for the key.
     * This method start search value from the smallest array.
     *
     * @param key - the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or {@code null} if this COLA contains no mapping for the key
     */
    V getTopDown(K key);

    /**
     * Associates the specified value with the specified key in this COLA.
     *
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     */
    void put(K key, V value);

    /**
     * Returns the number of key-value entries in this COLA.
     *
     * @return the number of key-value mappings in this COLA
     */
    long size();

    /**
     * COLA element entry
     *
     * @param key - the key whose associated value
     * @param value - value associated with specified key
     * @param <K> type of key
     * @param <V> type of value
     */
    record Entry<K extends Comparable<K>, V>(K key, V value) implements Comparable<Entry<K, V>> {
        @Override
        public int compareTo(Entry<K, V> o) {
            return key.compareTo(o.key);
        }
    }
}
