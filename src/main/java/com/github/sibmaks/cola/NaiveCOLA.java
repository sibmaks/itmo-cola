package com.github.sibmaks.cola;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sibmaks
 * @since 0.0.1
 */
public class NaiveCOLA<K extends Comparable<K>, V> implements COLA<K, V> {
    private final List<Entry<K, V>[]> arrays;
    private long size;

    public NaiveCOLA() {
        this.arrays = new ArrayList<>();
    }

    @Override
    public V getBottomUp(K key) {
        for (int i = arrays.size() - 1, j; i >= 0; i--) {
            var entries = arrays.get(i);
            if ((j = binSearch(key, entries)) != -1) {
                return entries[j].value();
            }
        }
        return null;
    }

    @Override
    public V getTopDown(K key) {
        int i;
        for (var entries : arrays) {
            if ((i = binSearch(key, entries)) != -1) {
                return entries[i].value();
            }
        }
        return null;
    }

    @Override
    public void put(K key, V value) {
        size++;
        var entry = new Entry<>(key, value);
        arrays.add(new Entry[]{entry});
        if(size > 1 && size % 2 == 0) {
            merge();
        }
    }

    @Override
    public long size() {
        return size;
    }

    /**
     * Simple binary search for an entry in an array given a key
     *
     * @param key - key of the element we are looking for
     * @param entries - array of elements in which try to find the key
     * @return index of the key in the given array or -1 if the search failed
     */
    private int binSearch(K key, Entry<K, V>[] entries) {
        int low = 0;
        int high = entries.length - 1;
        while (low <= high) {
            var middle = (low + high) >>> 1;
            var middleEntry = entries[middle];
            var middleKey = middleEntry.key();
            var val = middleKey.compareTo(key);
            if (val < 0) {
                low = middle + 1;
            } else if (val > 0) {
                high = middle - 1;
            } else {
                return middle;
            }
        }
        return -1;
    }

    /**
     * Checks whether Arrays with same length exist, if so, merges them and checks again (cascading).
     */
    private void merge() {
        int lastArrayLength = arrays.get(arrays.size() - 1).length;
        int toMerge = lastArrayLength;
        for(int i = arrays.size() - 2; i >= 0; i--) {
            if(arrays.get(i).length == toMerge) {
                toMerge <<= 1;
            } else {
                break;
            }
        }
        if(lastArrayLength == toMerge) {
            return;
        }
        var merged = new Entry[toMerge];
        while (toMerge > 0) {
            var entries = arrays.remove(arrays.size() - 1);
            merge(entries, merged, merged.length - toMerge);
            toMerge -= entries.length;
        }
        arrays.add(merged);
    }

    /**
     * Merge two sorted arrays
     *
     * @param left array to merge
     * @param destination destination array to save merged
     * @param destinationSize size of filled area of destination array
     */
    private void merge(Entry<K, V>[] left, Entry<K, V>[] destination, int destinationSize) {
        int leftPointer = 0;
        for(int i = 0, size = left.length + destinationSize; i < size; i++) {
            if(i >= destinationSize) {
                System.arraycopy(left, leftPointer, destination, i, left.length - leftPointer);
                break;
            } else {
                if(left[leftPointer].compareTo(destination[i]) > 0) {
                    continue;
                }

                int greaterIndex = find1stGreaterIndex(destination[i], left, leftPointer, left.length - 1);
                if(greaterIndex == -1) {
                    moveRight(destination, i, left.length - leftPointer);
                    System.arraycopy(left, leftPointer, destination, i, left.length - leftPointer);
                    break;
                }

                if(greaterIndex == 0) {
                    continue;
                }
                moveRight(destination, i, greaterIndex);
                System.arraycopy(left, leftPointer, destination, i, greaterIndex);
                leftPointer += greaterIndex;
                if(leftPointer == left.length) {
                    break;
                }
                i += greaterIndex;
            }
        }
    }

    private static<K extends Comparable<K>, V> int find1stGreaterIndex(Entry<K, V> great, Entry<K, V>[] array, int from, int to) {
        int ans = -1;
        while (from <= to) {
            var middle = (from + to) >>> 1;
            var middleEntry = array[middle];
            var val = middleEntry.compareTo(great);
            if (val <= 0) {
                from = middle + 1;
            } else {
                ans = middle;
                to = middle - 1;
            }
        }
        return ans;
    }

    /**
     * Move all elements in array, starting from specified position till end on shift positions.
     *
     * @param destination array to shift
     * @param from starting position (exclusive)
     * @param shift shift on
     */
    private static void moveRight(Entry[] destination, int from, int shift) {
        System.arraycopy(destination, from, destination, from + shift, shift);
    }
}
