package com.github.sibmaks.cola;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sibmaks
 * @since 0.0.1
 */
class NaiveCOLATest {

    @Test
    void testPutAndGetElement() {
        var cola = new NaiveCOLA<String, String>();
        assertEquals(0, cola.size());

        var key = UUID.randomUUID().toString();
        var value = UUID.randomUUID().toString();

        cola.put(key, value);
        assertEquals(1, cola.size());

        var bottomUp = cola.getBottomUp(key);
        assertEquals(value, bottomUp);

        var topDown = cola.getTopDown(key);
        assertEquals(value, topDown);
    }

    @Test
    void testPutAndGetElementsWithSameKey() {
        var cola = new NaiveCOLA<String, String>();

        var key1 = UUID.randomUUID().toString();
        var value1 = UUID.randomUUID().toString();

        cola.put(key1, value1);

        var key2 = UUID.randomUUID().toString();
        var value2 = UUID.randomUUID().toString();
        cola.put(key2, value2);

        var value3 = UUID.randomUUID().toString();
        cola.put(key1, value3);

        var bottomUp = cola.getBottomUp(key1);
        assertEquals(value3, bottomUp);

        var topDown = cola.getTopDown(key1);
        assertEquals(value1, topDown);
    }

    @Test
    void testGetBottomUpByNoExistsKeyInEmptyCOLA() {
        var cola = new NaiveCOLA<String, String>();

        var key = UUID.randomUUID().toString();

        var value = cola.getBottomUp(key);
        assertNull(value);
    }

    @Test
    void testGetBottomUpByNoExistsKey() {
        var cola = new NaiveCOLA<String, String>();

        cola.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());

        var key = UUID.randomUUID().toString();

        var value = cola.getBottomUp(key);
        assertNull(value);
    }

    @Test
    void testGetTopDownByNoExistsKeyInEmptyCOLA() {
        var cola = new NaiveCOLA<String, String>();

        cola.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());

        var key = UUID.randomUUID().toString();

        var value = cola.getTopDown(key);
        assertNull(value);
    }

}